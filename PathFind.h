#pragma once

template <class T>
class PathFind{
protected:
	const int MAX_TRACKING_COUNT = 40;

	virtual float distance(const T* from, const T* to) = NULL;
	virtual bool isReachable(const T* from, const T* to) = NULL;
	virtual std::vector<T*> getNodes(const T* target) = NULL;
public:
	struct PathNodeData
	{
		T* Node;
		bool bSeek;
		PathNodeData* parent;
		float distance;

	};
	PathNodeData* addPath(T* node, PathNodeData* p);
	std::stack<T*> FindPath(T* from, T* to);
};
