#include "stdafx.h"
#include "PathFind.h"

template <class T>
typename PathFind<T>::PathNodeData* PathFind<T>::addPath(T* node, typename PathFind<T>::PathNodeData* p) {
	PathNodeData* data = new PathNodeData();

	data->Node = node;
	if (p != nullptr)
	{
		data->parent = p;
		data->distance = p->distance + distance(node, p->Node);
	}
	else data->distance = 0;
	return data;
}

template <class T>
std::stack<T*> PathFind<T>::FindPath(T* from, T* to) {
	std::stack<T*> generateNode;

	std::vector<PathNodeData*> paths;

	generateNode.push(to);

	PathNodeData* cPath = addPath(from, nullptr);
	paths.push_back(cPath);
	bool isSeek = false;
	int precount = 0;
	while (++precount < MAX_TRACKING_COUNT)
	{
		cPath->bSeek = true;

		if (isReachable(cPath->Node, to))
		{
			while (cPath->parent != nullptr)
			{
				generateNode.push(cPath->Node);
				cPath = cPath->parent;
			}
			paths.clear();
			return generateNode;
		}

		for (auto n : getNodes(cPath->Node))
		{
			isSeek = false;
			for (auto data : paths) {
				if (data->Node->x == n->x && data->Node->y == n->y) {
					isSeek = true;
					break;
				}
			}
			if (!isSeek)
				paths.push_back(addPath(n, cPath));
		}

		float min = FLT_MAX;

		for (auto data : paths)
		{
			if (data->distance + distance(data->Node, to)< min && !data->bSeek)
			{
				cPath = data;
				min = data->distance + distance(data->Node, to);
			}
		}

	}

	generateNode.pop();
	while (cPath->parent != nullptr)
	{
		generateNode.push(cPath->Node);
		cPath = cPath->parent;
	}
	paths.clear();
	return generateNode;

}
